import Link from 'next/link'
import { hkGroteskRegular } from '../Fonts'

export const Navigation = () => {
  const buttonStyle = {
    height: '2rem',
    padding: '0 3rem',
  }
  return (
    <nav
      className={hkGroteskRegular.className}
      style={{
        display: 'flex',
        marginTop: '1em',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 100,
      }}
    >
      <Link href='/'>
        <img
          src='https://fotografierausch.de/wp-content/uploads/2021/05/Logo.png'
          style={{ height: 70, width: 237 }}
        />
      </Link>

      <div>
        <Link href='tel:+4915258097425'>
          <button style={buttonStyle}>+49 1525 8097425</button>
        </Link>
        <Link href='/'>
          <button style={buttonStyle}>STARTSEITE</button>
        </Link>
        <Link href='/about'>
          <button style={buttonStyle}>&Uuml;BER MICH</button>
        </Link>
        <Link href='/gallery'>
          <button style={buttonStyle}>PORTFOLIO</button>
        </Link>
        <Link href='/contact'>
          <button  className="bg-tertiary-100 text-primary-500" style={{ ...buttonStyle }}>
            KONTAKT
          </button>
        </Link>
      </div>
    </nav>
  )
}
