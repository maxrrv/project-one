import Link from 'next/link'
import { useState } from 'react'

export const MobileNavigation = () => {
  const [showMenu, setShowMenu] = useState(false)
  return (
    <nav
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: '10px',
      }}
    >
      <Link href='/'>
        <img
          src='https://fotografierausch.de/wp-content/uploads/2021/05/Logo.png'
          style={{ height: '50px', width: 'auto' }}
        />
      </Link>

      <div style={{ display: 'flex', alignItems: 'center', marginRight: '10px' }}>
        <Link href='whatsapp://send?text=Hallo Britta! &phone=+4915258097425'>
          <img src='/whatsapp_logo.svg' height='50' width='50' />
        </Link>
        <button onClick={() => setShowMenu(!showMenu)}>
          <img
            src='/icon-menu.svg'
            style={{
              transform: showMenu && 'rotate(-90deg)',
              transition: 'all .1s ease-in-out',
              height: '1.5em',
              fontSize: '1.2em',
              lineHeight: '1.5em',
              padding: '2px',
            }}
          />
        </button>
      </div>
      <div
        style={{
          display: showMenu ? 'flex' : 'none',
          width: showMenu ? 'auto' : 0,
          position: 'fixed',
          background: '#fff',
          padding: '1rem',
          right: 0,
          top: 50,
          bottom: 0,
          zIndex: 10,
          flexDirection: 'column',
          alignItems: 'end',
          justifyContent: 'start',
          fontSize: '1.2em',
          transition: 'all .1s ease-in-out',
        }}
      >
        <Link href='/' style={{ marginBottom: '1rem' }}>
          <button>STARTSEITE</button>
        </Link>
        <Link href='/gallery' style={{ marginBottom: '1rem' }}>
          <button>GALLERIE</button>
        </Link>
        <Link href='/about' style={{ marginBottom: '1rem' }}>
          <button>&Uuml;BER MICH</button>
        </Link>
        <Link href='/contact' style={{ marginBottom: '1rem' }}>
          <button>KONTAKT</button>
        </Link>
      </div>
    </nav>
  )
}
