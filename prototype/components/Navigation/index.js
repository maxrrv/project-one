import { MobileNavigation } from './MobileNavigation'
import { Navigation as DesktopNavigation } from './Navigation'

export const Navigation = () => (
  <div>
    <div className='block lg:hidden'>
      <MobileNavigation />
    </div>
    <div className='hidden lg:block'>
      <DesktopNavigation />
    </div>
  </div>
)
