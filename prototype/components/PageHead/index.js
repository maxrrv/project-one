import Head from 'next/head'

export const PageHead = () => (
  <Head>
    <link rel='preconnect' href='https://fonts.googleapis.com' />
    <link rel='preconnect' href='https://fonts.gstatic.com' crossorigin />
    <link
      href='https://fonts.googleapis.com/css2?family=Lemonada&family=Manrope:wght@300&family=Overpass:wght@200&family=Sansita+Swashed:wght@300&family=Truculenta:wght@300&display=swap'
      rel='stylesheet'
    />

    <link rel='icon' href='/favicon.ico' />
    <meta name='description' content='tierfotografierausch.de' />
    <meta
      name='viewport'
      content='width=device-width, target-densitydpi=device-dpi, initial-scale=1'
    />
  </Head>
)
