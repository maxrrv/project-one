import localFont from 'next/font/local'

const didonesque = localFont({ src: 'Didonesque-Roman.woff' })
const secuela = localFont({ src: 'Secuela-Medium.woff' })
const hkGroteskRegular = localFont({ src: 'HKGrotesk-Regular.woff' })
const hkGroteskSemiBold = localFont({ src: 'HKGrotesk-SemiBold.woff' })
const highSummit = localFont({ src: 'High Summit.otf' })

export { didonesque, highSummit, hkGroteskRegular, hkGroteskSemiBold, secuela }
