import Link from 'next/link'
import { hkGroteskRegular } from '../Fonts'

export const Footer = () => (
  <footer
    className={hkGroteskRegular.className}
    style={{ display: 'flex', justifyContent: 'space-between', marginTop: '2rem' }}
  >
    <span>&copy;{new Date().getFullYear()} Fotografierausch</span> <Link href='/contact'>Kontakt</Link>{' '}
    <Link href='/imprint'>Impressum</Link>{' '}
    <a href='https://www.instagram.com/fotografierausch/' target='_blank'>
      <img src='/instagram_logo.svg' alt='instagram' width='30' height='30' />
    </a>
    <div>
    </div>
  </footer>
)
