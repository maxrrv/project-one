export const Spacer = ({ className }) => <div className={className} style={{ height: '1.5em' }}></div>
