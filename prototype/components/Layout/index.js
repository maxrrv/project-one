import { Analytics } from '@vercel/analytics/react'
import { Footer } from '../Footer'
import { Navigation } from '../Navigation'
import { PageHead } from '../PageHead'

export default function Layout({ children }) {
  return (
    <div style={{ width: '100vw', display: 'flex', justifyContent: 'center' }}>
      <PageHead />
      <Analytics />
      <div
        style={{
          minHeight: '95vh',
          display: 'flex',
          flexDirection: 'column',
          width: '98vw',
          justifyContent: 'center',
        }}
      >
        <div style={{ flexBasis: 'auto' }}>
          <Navigation />
        </div>
        <div style={{ flexBasis: 'auto', flexGrow: 1 }}>
          <main style={{ height: '100%' }}>{children}</main>
        </div>
        <div style={{ flexBasis: 'auto' }}>
          <Footer />
        </div>
      </div>
    </div>
  )
}
