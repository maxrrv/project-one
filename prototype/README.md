# Prototype Project One

wireframe: https://wireframe.cc/hsf8uM
https://fonts.google.com/specimen/Sansita+Swashed?subset=latin&category=Serif,Sans+Serif,Display&vfonly=true
https://fonts.google.com/specimen/Truculenta?subset=latin&category=Serif,Sans+Serif,Display&vfonly=true
https://fonts.google.com/specimen/Lemonada?subset=latin&category=Serif,Sans+Serif,Display&vfonly=true

# todo

create backend  
decide and configure cms  
decide where to host  
configure project on host

# inspiration

http://www.bradjameswildlifephotography.com/
http://www.susankmcconnell.com/
https://davidlloyd.net/
https://www.craigjoneswildlifephotography.co.uk/

## notes
### prio 1
* mobile view image should be extending bigger
* add design elements from wedding page to animal page

### prio 2 
* recolor contact link
* first only dogs and horses

### prio 3
* add slider that is clickable and changes pictures after a timer
* add small circle in corner that indicates when the next image loads
