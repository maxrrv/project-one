import Head from "next/head";
import Layout from "../components/Layout";
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation } from 'swiper';

import 'swiper/css';
import 'swiper/css/navigation';

export default function Home() {
  return (
    <Layout>
      <Head>
        <title>Startseite | Tierfotografierausch</title>
      </Head>
    <Swiper
       modules={[Navigation]}
    spaceBetween={10}
       slidesPerView={2}
       navigation
       onSwiper={(swiper) => console.log(swiper)}
       onSlideChange={() => console.log('slide change')}
    className="my-swiper"
    >
      <SwiperSlide><img src={'./images/dog-1.jpg'} /></SwiperSlide>
      <SwiperSlide><img src={'./images/dog-2.jpg'} /></SwiperSlide>
      <SwiperSlide><img src={'./images/dog-3.jpg'} /></SwiperSlide>
      <SwiperSlide><img src={'./images/dog-4.jpg'} /></SwiperSlide>
    </Swiper>
    </Layout>
  );
}
