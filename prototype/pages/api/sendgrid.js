import sendgrid from "@sendgrid/mail";

const SENDER_IDENTITY = "maxrrv.coder@gmail.com"
const RECEIVER_INBOX = "info@fotografierausch.de"

sendgrid.setApiKey(process.env.SENDGRID_API_KEY);


async function sendEmail(req, res) {
  try {
    // console.log("REQ.BODY", req.body);
    await sendgrid.send({
      to: RECEIVER_INBOX, // Your email where you'll receive emails
      from: SENDER_IDENTITY, // your website email address here
      subject: `${req.body.subject}`,
      html: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html lang="en">
          <head>
            <meta charset="utf-8">
          
            <title>Eine neue Nachricht vom Tierfotografierausch Kontaktformular!</title>
            <meta name="description" content="Kontaktanfrage">
            <meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
            <link rel="stylesheet" href="css/styles.css?v=1.0">
          
          </head>
          <body>
            <div class="img-container" style="display: flex;justify-content: center;align-items: center;border-radius: 5px;overflow: hidden; font-family: 'helvetica', 'ui-sans';">              
              </div>
                <div class="container" style="margin-left: 20px;margin-right: 20px;">
                  <h3>Du hast eine neue Nachricht von ${req.body.fullname}</h3>
                  <div> Email: ${req.body.email} </div>
                  <div style="font-size: 16px;">
                  <p>Nachricht:</p>
                  <p>${req.body.message}</p>
              </div>
            </div>
          </body>
        </html>`,
    });
  } catch (error) {
    console.log(error);
    return res.status(error.statusCode || 500).json({ error: error.message });
  }

  return res.status(200).json({ error: "" });
}

export default sendEmail;

