import Head from "next/head";
import { Fragment, useState } from "react";
import Layout from "../components/Layout";

const Gallery = () => {
  const [modalImage, setModalImage] = useState("");
  const images = [
    "images/dog-2.jpg",
    "images/dog-3.jpg",
    "images/dog-4.jpg",
    "images/dog-5.jpg",
    "images/dog-1.jpg",
  ];

  const getPreviousImage = () => {
    if (modalImage === 0) {
      return images.length - 1;
    }

    return (modalImage - 1) % images.length;
  };

  const Modal = () => (
    <div
      style={{
        position: "absolute",
        minHeight: "100vh",
        height: "100%",
        width: "100vw",
        top: 0,
        left: 0,
        backgroundColor: "rgba(51, 58, 51, 0.8)",
        zIndex: 1,
        overflowY: "scroll",
      }}
    >
      <div
        style={{
          position: "fixed", // very useful
          height: "100vh",
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <button
          onClick={() => setModalImage(getPreviousImage())}
          style={{ position: "fixed", top: 80, bottom: 80, left: 0 }}
        >
          <img src="/arrow_left.svg" alt="previous image" height="50px" width="50px" />
        </button>
        <img
          src={images[modalImage]}
          style={{ maxHeight: "600px", maxWidth: "100%", width: "auto" }}
        />
        <button
          onClick={() => setModalImage((modalImage + 1) % images.length)}
          style={{ position: "fixed", top: 80, bottom: 80, right: 0 }}
        >
          <img src="/arrow_right.svg" alt="next image" height="50px" width="50px" />
        </button>
        <button
          onClick={() => setModalImage("")}
          style={{ position: "fixed", top: 10, right: 10 }}
        >
          <img src="/cross.svg" alt="close button" height="50px" width="50px" />
        </button>
      </div>
    </div>
  );

  return (
    <div style={{ position: "relative" }}>
      {images[modalImage] ? <Modal /> : <Fragment />}
      <Layout>
        <Head>
          <title>Gallerie | Tierfotografierausch</title>
        </Head>
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "repeat(auto-fill, minmax(200px, 1fr))",
            gridGap: 10,
          }}
        >
          {images.map((image, index) => (
            <div style={{ position: "relative", paddingBottom: "100%" }}>
              <img
                src={image}
                style={{
                  position: "absolute",
                  width: "100%",
                  height: "100%",
                  top: 0,
                  left: 0,
                  objectFit: "cover",
                }}
                alt="doggo"
                onClick={() => setModalImage(index)}
              />
            </div>
          ))}
        </div>
      </Layout>
    </div>
  );
};

export default Gallery;
