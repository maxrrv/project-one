import Head from "next/head";
import Layout from "../components/Layout";
import { Spacer } from "../components/Spacer";
import { secuela, didonesque, hkGroteskRegular, hkGroteskSemiBold, highSummit } from '../components/Fonts'

const TextImageComponent = ({ imageLink, text, header, reverse }) => {
  return (
    <>
    <div
      style={{
        display: "flex",
        flexDirection: reverse ? "row-reverse" : "row",
        flexWrap: "wrap",
        justifyContent: "space-around",
      }}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
        className="w-screen lg:w-3/5"
      >
        <h2 className={ highSummit.className } style={{ textAlign: "center", fontSize: '3rem' }}>{header}</h2>
        <p
          className={ hkGroteskRegular.className }
          style={{
            textAlign: "center",
            overflowWrap: "break-word",
            fontSize: "1.2rem",
            padding: "1rem",
          }}
        >
          {text}
        </p>
      </div>
      <Spacer />
      <Spacer />
      <img
        src={imageLink}
        className="w-screen lg:w-2/5"
        style={{
          maxHeight: "70vh",
          objectFit: "cover",
        }}
      />
    </div>
    <Spacer />
    </>
  );
};

const About = () => {
  return (
    <Layout>
      <Head>
        <title>&Uuml;ber mich | Tierfotografierausch</title>
      </Head>
      <Spacer />
      <div
          className="hidden lg:block"
        style={{
          height: "50vh",
          width: "100%",
        }}
      >
        <img
          style={{
            width: "100%",
            height: "100%",
            objectFit: "cover",
          }}
          src="https://fotografierausch.de/wp-content/uploads/2021/12/7-hochzeitsfotografin-brittarausch-kassel.jpg"
        />
      </div>
      <Spacer />
      <div style={{ display: "flex", flexDirection: "column" }}>
        <TextImageComponent
          header="Hallo ich bin Britta und leidenschaftliche Tierfotografin"
          imageLink="https://fotografierausch.de/wp-content/uploads/2021/05/31-fotografierausch-ueber-mich-683x1024.jpg"
          text={
            <span>
              An dieser Stelle höre ich meistens auf, die ausführlichen
              Lebensläufe zu lesen. Ihr sucht eine einfühlsame Persönlichkeit,{" "}
              die euch in Szene setzt, ob bei einem Tiershooting oder dem
              schönsten Waldspaziergang ever.
            </span>
          }
        />
        <TextImageComponent
          reverse
          header="Als Fotografin bin ich sowohl stille Beobachterin"
          imageLink="https://fotografierausch.de/wp-content/uploads/2021/05/11fototgrafierausch-pferd-683x1024.jpg"
          text={
            <span>
              als auch die, die Akzente setzt und dabei werdet ihr euch
              wohlfühlen. An eurem Ehrentag sind die Spots auf euch gerichtet,{" "}
              ich rücke euch ins rechte Licht und halte unvergessliche
              Erinnerungen fest. Dabei müsst ihr euch keine Sorgen machen dass{" "}
              eure Vierbeiner das nicht schaffen, auf meiner Website findet ihr{" "}
              keine Bilder mit professionellen Tieren sondern normale Vierbeiner{" "}
              die teils zum ersten mal vor der Kamera standen.
            </span>
          }
        />
        <TextImageComponent
          header="Schreibt mir!"
          imageLink="https://fotografierausch.de/wp-content/uploads/2021/05/38-fotografierausch-trio-kassel-768x512.jpg"
          text={
            <div>
              Schickt mir einfach über mein Kontaktformular eine Nachricht, oder schreibt mir 
              eine WhatsApp &uuml;ber die oben stehende Nummer. Ich
              melde mich zeitnah bei euch und wer weiß, vielleicht lernen wir
              uns schon bald persönlich kennen.
            </div>
          }
        />
      </div>
    </Layout>
  );
}

export default About
