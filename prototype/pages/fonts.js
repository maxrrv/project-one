import Head from "next/head";
import Layout from "../components/Layout";
import { Spacer } from "../components/Spacer";
import { didonesque, secuela, hkGroteskRegular, hkGroteskSemiBold, highSummit } from "../components/Fonts";
export default function Fonts() {
  return (
    <Layout>
      <Head>
        <title>Fonts | Tierfotografierausch</title>
      </Head>
<span className={didonesque.className}>The quick brown fox jumps over the lazy dog.</span>
      <Spacer />
<span className={secuela.className}>The quick brown fox jumps over the lazy dog.</span>
      <Spacer />
<span className={hkGroteskSemiBold.className}>The quick brown fox jumps over the lazy dog.</span>
      <Spacer />
<span className={hkGroteskRegular.className}>The quick brown fox jumps over the lazy dog.</span>
      <Spacer />
<span className={highSummit.className}>The quick brown fox jumps over the lazy dog.</span>
    {['primary', 'secondary', 'tertiary'].map(variant => (
      <div>
        <div className={`bg-${variant}-100 h-24 w-24 inline-block`}>Hello</div>
        <div className={`bg-${variant}-200 h-24 w-24 inline-block`}>Hello</div>
        <div className={`bg-${variant}-300 h-24 w-24 inline-block`}>Hello</div>
        <div className={`bg-${variant}-400 h-24 w-24 inline-block`}>Hello</div>
        <div className={`bg-${variant}-500 h-24 w-24 inline-block`}>Hello</div>
      </div>
    ))}
    </Layout>
  );
}

