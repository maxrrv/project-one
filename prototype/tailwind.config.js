module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    colors: {
      primary:{
        100: "#92BB36",	
        200: "#EDFBCD",
        300: "#CBF07B",
        400: "#5A7E0B",
        500: "#151E00",
      },
      secondary: {
        100: "#C7AB3A",
        200: "#FFF6D0",
        300: "#FFE682",
        400: "#866E0B",
        500: "#201A00",
      },
      tertiary: {
        100: "#247578",
        200: "#BEE7E9",
        300: "#50999C",
        400: "#074E51",
        500: "#001314",
      },
    },
  },
  plugins: [],
}
